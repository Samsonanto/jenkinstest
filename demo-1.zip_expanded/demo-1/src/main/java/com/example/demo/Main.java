package com.example.demo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Main {

	
	public static void main(String[] args) throws IOException {
		
		Add add = new Add();
		add.setData(1, 2, 3);
		
		add.cal();
		FileOutputStream os =  new FileOutputStream("file.bin");
		ObjectOutputStream o = new ObjectOutputStream(os);
		
		o.writeObject(add);
		o.close();
		
		System.out.println("File is seeralized");
		
		
	}
	
}
