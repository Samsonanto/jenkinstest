package com.example.demo;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin
@RestController
public class Controller {

	@Autowired
	StudentBeanDAO sDAO;
	
	@Autowired
	MarksDAO mDAO;
	
	
	@PostMapping("/Student")
	public String addStudent(@RequestBody StudentBean s)
	{
		sDAO.save(s);
		
		return "Saved";
		
	}
	
	
	@PostMapping("/Marks")
	public String addMarks(@RequestBody MarksBean m) {
		
		mDAO.save(m);
		
		return "Saved Marks";
	}
	
	
	@GetMapping("/Student")
	public List<StudentBean> getAllStudent(){
		
		return (List<StudentBean>)sDAO.findAll();
	}
	
	@GetMapping("/Marks")
	public List<MarksBean> getAllMarks(){
		return  (List<MarksBean>) mDAO.findAll();
	}
	
	@DeleteMapping("/Student/{id}")
	public String deleteStudentById(@PathVariable("id") int regno) {
		
		sDAO.deleteById(regno);
		
		return "Deleted Sucessfully";
	}
	
	@PutMapping("/Student/{id}")
	public String updateStudentById(
			@RequestParam("regno") int regno,
			@RequestParam("fname") String fname,
			@RequestParam("lname") String lname,
			@RequestParam("citycode") int citycode,
			@RequestParam("dob") @DateTimeFormat(pattern="yyyy-mm-dd") Date dob
			) {
		
		Optional<StudentBean> s = sDAO.findById((regno));
		
		s.get().setCitycode(citycode);
		s.get().setDob(dob);
		s.get().setFname(fname);
		s.get().setLname(lname);
		s.get().setRegno(regno);		
		
		sDAO.save(s.get());
		
		return "Updated Sucessufully";
	}
	
	
//	
//	@GetMapping("/Marks/{id}")
//	public MarksBean getMarks(@PathVariable String id) {
//		
//		
//		return null;
//		
//	}
	
	
}
