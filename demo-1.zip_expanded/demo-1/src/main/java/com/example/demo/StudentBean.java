package com.example.demo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="Student")
public class StudentBean {

	@Id
	private int regno;
	@Column
	private String fname;
	@Column
	private String lname;
	@Column
	private Date dob ;
	@Column
	private int citycode;
	
	@Transient
	Set<MarksBean> s = new HashSet<>();  
	
	@OneToMany(mappedBy = "regno")
	public Set<MarksBean> getS() {
		return s;
	}

	public void setS(Set<MarksBean> s) {
		this.s = s;
	}

	public StudentBean() {
		super();
	}
	


	

	public StudentBean(int regno, String fname, String lname, Date dob, int citycode, Set<MarksBean> s) {
		super();
		this.regno = regno;
		this.fname = fname;
		this.lname = lname;
		this.dob = dob;
		this.citycode = citycode;
		this.s = s;
	}

	public int getRegno() {
		return regno;
	}

	public void setRegno(int regno) {
		this.regno = regno;
	}

	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public int getCitycode() {
		return citycode;
	}
	public void setCitycode(int citycode) {
		this.citycode = citycode;
	}
	
	
}
