package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WelcomeApplicationTests {

	
	@Autowired
	DAO dao;
	
	@Autowired
	Controller c;
	
	
	@Test
	public void daoTest() {
		
		
		assertTrue(dao.getS() != null);		
	}
	
	
	@Test
	public void controllerTest() {
		
		assertTrue(c.getWelcome() != null);
		
	}

}
