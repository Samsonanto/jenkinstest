package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Service
public class Controller {

	
	@Autowired
	DAO dao;
	
	@GetMapping("/welcome")
	public List<NameBean> getWelcome() {
		
		return dao.getS();		
	}
	
}
