package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class DAO {

	private List<NameBean> s ;

	public List<NameBean> getS() {
		return s;
	}

	public void setS(List<NameBean> s) {
		this.s = s;
	}

	
	
	public DAO() {
		super();
		this.s = new ArrayList<>();
		
		s.add(new NameBean("Samson"));
		s.add(new NameBean("Karan"));
		s.add(new NameBean("Siddant"));
		s.add(new NameBean("Ishbir"));
		s.add(new NameBean("Gunjan"));
		
	}

	public DAO(List<NameBean> s) {
		super();
		this.s = s;
	}
	
	
	
	
	
	
	
}
